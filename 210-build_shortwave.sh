#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2023 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Build Shortwave.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

bash_d_include error

### variables ##################################################################

# Nothing here.

### functions ##################################################################

# Nothing here.

### main #######################################################################

if $CI; then # break in CI, otherwise we get interactive prompt by JHBuild
  error_trace_enable
fi

jhb run meson setup \
  --prefix "$VER_DIR" \
  "$BLD_DIR/swave" \
  "$VER_DIR/shortwave-3.2.0"

jhb run meson compile -C "$BLD_DIR/swave"
#jhb run meson install -C "$BLD_DIR/swave"
